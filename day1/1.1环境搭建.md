# 1.1环境搭建

<a name="7GURx"></a>
#### 课前知晓：
Vue-Cli的各个版本不一样，现在我们所使用的是Vue-cli4版本。整个构建目录的变化及webpack的升级，提升了构建项目速度。一个可视化图形界面方便你去创建、更新和管理项目的方方面面。
<a name="VmeMC"></a>
#### 初始化
安装最新的Vue-Cli4
```git
npm install @vue/cli -g
#OR
yarn global add @vue/cli
vue --version //查看版本证明安装成功
```
通过vue ui 创建项目
```git
vue ui
```
自动打开浏览器，接下来的操作如图所示：
![0603](https://images.gitee.com/uploads/images/2020/0603/110338_54173198_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/110527_2bbcdb29_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/110527_2bbcdb29_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_f8e938eb_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_f8e938eb_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_5ebf2f52_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_4a66ba0d_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_a8a33a17_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_6ab10cd7_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111425_b7a80cf9_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_03c961ac_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_b54f05f6_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_1437c442_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_ef8be883_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_a8b40bfb_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111427_de6771c3_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_aa3ad011_1720749.png)
![0603](https://images.gitee.com/uploads/images/2020/0603/111426_71cbdcc7_1720749.png)
```git
src文件
  |- api          存放接口数据
  |- assets       存放静态资源
  |- components   公用组件
  |- plugins      生成的插件
  |- config       存放配置文件
  |- router       路由管理
  |- store        vuex配置
  |- utils        存放工具方法
  |- views        vue视图文件
  |   App.vue     根组件
  |   main.js     入口文件
```
